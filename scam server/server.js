const http = require('http');
const fs = require('fs').promises;

const hostname = '127.0.0.1';
const port = 8080;

const server = http.createServer((req, res) => {
    console.log(req.url)
    saveNewCookie(req.url)
});

server.listen(port, hostname, () => {
    console.log(`Server running at http://${hostname}:${port}/`);
});

function saveNewCookie(cookie) {
    fs.appendFile('cookie.txt', cookie + "\n")
  }

