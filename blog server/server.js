const http = require('http');
const fs = require('fs').promises;
var commentsFile = require('./comments.json');

const hostname = '127.0.0.1';
const port = 3000;

const server = http.createServer((req, res) => {
    if (req.method === 'GET' && req.url === '/getComments'){
        fs.readFile(__dirname + "/comments.json").then(contents => {
            res.setHeader("Content-Type", "text/json");
            let data = JSON.parse(contents);
            res.write(JSON.stringify(data));
            res.end();
        })
    }
    else if(req.method === 'POST' && req.url === '/addComment'){
        req.on('data', function (data) {
            saveNewComment(data.toString("utf-8"))
        })
    }
    else{
        fs.readFile(__dirname + "/vulnarable.html").then(contents => {
            res.writeHead(200, {
                'Set-Cookie': ('auth:' + setCookie()),
                'Content-Type': 'text/html'
              });
            res.end(contents);
        })
    }
});

server.listen(port, hostname, () => {
    console.log(`Server running at http://${hostname}:${port}/`);
});

function saveNewComment(comment) {
    commentsFile.push(comment)
    fs.writeFile('comments.json', JSON.stringify(commentsFile))
  }


function setCookie(){
    var cookiesChars = "1234567890qwertyuiopasfghjklzxcvbnm";
    var authKey = "";
    for(let i =0; i <= 64; i++){
        let randomchar = cookiesChars.charAt(Math.floor(Math.random() * cookiesChars.length))
        authKey += randomchar
    }
    return authKey;
}